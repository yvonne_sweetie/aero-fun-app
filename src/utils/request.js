// 基于 axios 封装网络请求

import theAxios from 'axios'
import router from '@/router'
import { Notify } from 'vant'
import { getToken, delToken } from '@/utils/token.js'

const axios = theAxios.create({
  baseURL: 'http://36.129.58.119:8090',
  timeout: 20000 // 20s超时时间（请求20s无响应直接判定超时）
})

// 添加请求拦截器
axios.interceptors.request.use(
  function (config) {
    // 在发送请求之前做些什么
    // 目标：统一携带token
    // 判断本地有token再携带，判断具体api/index.js里如果没有携带Authorization，我再添加上去
    // 未定义叫undefine，null具体的值你得赋予才叫空
    // ?. 可选链操作符，如果前面对象没有length，原地返回undefined
    // 如果getToken()在原地有值token字符串，才能调用length获取长度，否则报错

    if (
      getToken() &&
      getToken().length > 0 &&
      config.headers.Authorization === undefined
    ) {
      config.headers.Authorization = getToken()
    }
    return config
  },
  function (error) {
    // 对请求错误做些什么
    return Promise.reject(error)
  }
)

// 添加响应拦截器
// 本质：一个函数
axios.interceptors.response.use(
  function (response) {
    // http响应状态码为2xx,3xx就进入这里
    // 对响应数据做点什么

    // 用于解决本工程特有问题，即http响应状态码为200，但是内层自定义状态码为401，此时代表身份过期，需要跳转到登录
    if (response.data.code === 401) {
      Notify({ type: 'warning', message: '身份验证已过期，请重新登录！' })
      delToken()
      router.replace(`/login?path=${router.currentRoute.fullPath}`)
    } else {
      return response
    }
  },
  function (error) {
    // http响应状态码为4xx,5xx报错进入这里
    // 对响应错误做点什么
    console.dir('响应拦截器报错', error)
    return Promise.reject(error)
  }
)

export default ({
  url,
  method = 'GET',
  params = {},
  data = {},
  headers = {}
}) => {
  return axios({
    url,
    method,
    params,
    data,
    headers
  })
}
