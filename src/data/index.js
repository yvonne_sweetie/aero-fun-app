// 基地址
const baseUrl = 'http://www.yue606.com:8082'
// 首页 - 图片轮播图
const imgList = [
  require('@/assets/images/swipe/1.jpg'),
  require('@/assets/images/swipe/2.jpg'),
  require('@/assets/images/swipe/3.jpg')
]
// 首页 - 金刚区图标
const iconList = [
  { img: require('@/assets/images/icon/1.png'), text: '午餐' },
  { img: require('@/assets/images/icon/2.png'), text: '晚餐' },
  { img: require('@/assets/images/icon/3.png'), text: '夜宵' },
  { img: require('@/assets/images/icon/4.png'), text: '超市' }
]
// 首页 - 餐厅列表
const canteenList = [
  {
    id: '01',
    name: '所本部食堂',
    thumb: require('@/assets/images/canteen/01.png')
  },
  {
    id: '02',
    name: '铁岭露天台食堂',
    thumb: require('@/assets/images/canteen/02.png')
  },
  {
    id: '03',
    name: '高坎基地食堂',
    thumb: require('@/assets/images/canteen/03.png')
  }
]

// 点餐 - 选项
/* const dishOptions = [
  // 级联选择器选项
  {
    text: '所本部食堂',
    value: '01',
    children: [
      { text: '午餐', value: '午餐id' },
      { text: '晚餐', value: '晚餐id' }
    ]
  },
  {
    text: '铁岭露天台食堂',
    value: '02',
    children: [
      { text: '午餐', value: '午餐id' },
      { text: '晚餐', value: '晚餐id' }
    ]
  },
  {
    text: '高坎基地食堂',
    value: '03',
    children: [
      { text: '午餐', value: '午餐id' },
      { text: '晚餐', value: '晚餐id' }
    ]
  }
] */

// const orderCatList = [
//   { text: '午餐', value: 1 },
//   { text: '晚餐', value: 2 }
// ]

// 订单状态
const orderStatusList = [
  {
    id: 1,
    orderStatusId: 1,
    orderStatus: '待接单'
  },
  {
    id: 2,
    orderStatusId: 2,
    orderStatus: '已接单'
  },
  {
    id: 4,
    orderStatusId: 4,
    orderStatus: '已完成'
  },
  {
    id: 5,
    orderStatusId: 5,
    orderStatus: '已取消'
  },
  {
    id: -1,
    orderStatusId: -1,
    orderStatus: '全部'
  }
]

// 订单列表
const orderList = [
  {
    canteen: '所本部食堂',
    orderTime: '2023/04/20 10:00:23',
    amount: '24.00',
    orderStatusId: 0,
    orderStatus: '待接单'
  },
  {
    canteen: '高坎基地食堂',
    orderTime: '2023/05/20 10:00:23',
    amount: '24.00',
    orderStatusId: 1,
    orderStatus: '已接单'
  },
  {
    canteen: '所本部食堂',
    orderTime: '2023/06/21 10:00:23',
    amount: '24.00',
    orderStatusId: 2,
    orderStatus: '已完成'
  },
  {
    canteen: '铁岭露天台食堂',
    orderTime: '2023/04/20 10:00:23',
    amount: '24.00',
    orderStatusId: 3,
    orderStatus: '已取消'
  }
]

// 分类列表
const catList = [
  {
    cat_id: '01010001',
    cat_name: '套餐',
    canteen_id: '01'
  },
  {
    cat_id: '01010002',
    cat_name: '主菜',
    canteen_id: '01'
  },
  {
    cat_id: '01010003',
    cat_name: '主食',
    canteen_id: '01'
  },
  {
    cat_id: '01010004',
    cat_name: '小吃',
    canteen_id: '01'
  },
  {
    cat_id: '01010005',
    cat_name: '饮料',
    canteen_id: '01'
  }
]

// 菜品列表
// const dishList = [
//   {
//     dish_id: '01010001',
//     dish_name: '红烧肉套餐',
//     cat_id: '01010001',
//     canteen_id: '01',
//     dish_price: '12.00',
//     dish_number: 20,
//     dish_image: require('@/assets/images/dish/demo1.jpg'),
//     dish_description: '红烧肉150g，肉丝蒜苔150g，干锅娃娃150g，米饭100g',
//     amount: 0
//   },
//   {
//     dish_id: '01010002',
//     dish_name: '小鸡炖蘑菇套餐',
//     cat_id: '01010001',
//     canteen_id: '01',
//     dish_price: '12.00',
//     dish_number: 20,
//     dish_image: require('@/assets/images/dish/demo2.png'),
//     dish_description: '小鸡炖蘑菇150g，肉丝蒜苔150g，干锅娃娃150g，米饭100g',
//     amount: 0
//   },
//   {
//     dish_id: '01010003',
//     dish_name: '虾滑煲套餐',
//     cat_id: '01010001',
//     canteen_id: '01',
//     dish_price: '12.00',
//     dish_number: 20,
//     dish_image: require('@/assets/images/dish/demo3.png'),
//     dish_description: '虾滑煲150g，肉丝蒜苔150g，干锅娃娃150g，米饭100g',
//     amount: 0
//   }
// ]
// 菜品默认图片
const dishImage = require('@/assets/images/dish/demo2.png')
// 全局主色
const themeColor = '#ff853b'
// 用户默认头像
const avatar = require('@/assets/images/user/default.png')

export default {
  baseUrl, // 基地址
  imgList, // 首页 - 图片轮播缺省图片
  iconList, // 首页 - 金刚区图标
  orderStatusList, // 订单状态列表
  orderList,
  catList, // 分类列表
  dishImage,
  themeColor,
  canteenList,
  avatar
}
