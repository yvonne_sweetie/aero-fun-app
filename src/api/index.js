// 统一封装接口方法,
// 每个方法负责请求一个url地址
// 逻辑页面，导入这个接口方法，就能发起请求
// 优点：请求url路径，可以在这里统一管理
// 只负责调用一个接口, 返回一个Promise对象

import axios from '@/utils/request.js'

// 微信授权登录
export const WXAuthorizationAPI1 = ({ url }) =>
  axios({
    url: '/oauth2user',
    params: {
      resultUrl: url
    }
  })

// 用户 - 登录接口
export const loginAPI = ({ username, password }) =>
  axios({
    url: '/login',
    method: 'POST',
    data: {
      username,
      password
    }
  })

// 查询用户列表
export const getUserList = () =>
  axios({
    url: '/system/user/list'
  })
// 根据用户ID查询用户,默认查询管理员
export function getUserById(userId) {
  return axios({
    url: '/system/user/' + userId
  })
}
// 获取当前用户信息
export const getUser = () =>
  axios({
    // url: '/getInfo'
    url: '/system/user/userInfo'
  })

// 查询食堂列表
export const getCanteenList = () =>
  axios({
    url: '/order/canteen/list'
  })

// 查询菜品分类列表
export const getCatList = ({ canteenId }) =>
  axios({
    url: '/order/category/list',
    params: {
      canteenId
    }
  })
// 查询菜品列表
export const getDishList = ({ catId }) =>
  axios({
    url: '/order/dish/list',
    params: {
      catId
    }
  })

// 查询订单列表
export const getOrderList = ({ userId }) =>
  axios({
    url: '/order/order/list',
    params: {
      userId
    }
  })

// 查询订单明细列表
export const getOrderDetailList = ({ orderId }) =>
  axios({
    url: '/order/detail/list',
    params: { orderId }
  })

// 查询订单分类列表：如午餐、晚餐等
export const getOrderCategoryList = (canteenId) =>
  axios({
    url: '/order/orderCategory/canteen/' + canteenId
  })

// 查询配送地点列表
// 根据canteenId和UserName获取
export const getAddressList = ({ canteenId, userId }) =>
  // axios({
  //   url: '/order/address/info',
  //   params: {
  //     canteenId,
  //     userName
  //   }
  // })
  axios({
    url: '/order/address/deptAddressInfo',
    params: {
      canteenId,
      userId
    }
  })

// 新增订单
export const addOrder = (data) =>
  axios({
    url: '/order/order',
    method: 'POST',
    data: data
  })

// 取消订单
export const cancelOrder = ({ orderId }) =>
  axios({
    url: '/order/order/cancel',
    method: 'PUT',
    data: {
      orderId: orderId
    }
  })
// 完成订单
export const finishOrder = ({ orderId }) =>
  axios({
    url: '/order/order',
    method: 'PUT',
    data: {
      orderId: orderId,
      orderStatus: 4
    }
  })
