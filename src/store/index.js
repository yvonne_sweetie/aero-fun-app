import {
  getUser,
  getCanteenList,
  getCatList,
  getDishList,
  getOrderCategoryList
} from '@/api'
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    // 当前用户
    user: {},
    // 食堂列表
    canteenList: [],
    // 购物车
    cart: {},
    // 合计
    sum: 0,
    // 菜品分类
    categoryList: [],
    // 订单信息
    order: {}
  },
  mutations: {
    RECEIVE_CANTEEN(state, canteenObj) {
      state.order.canteen = canteenObj
      console.log('RECEIVE_CANTEEN 执行完了。')
    },
    RECEIVE_ORDER_CATEGORY(state, orderCategoryObj) {
      state.order.category = orderCategoryObj
      console.log('RECEIVE_ORDER_CATEGORY 执行完了。')
    },
    RECEIVE_DISHES(state, orderDishes) {
      state.order.dishes = orderDishes
      console.log('RECEIVE_DISHES 执行完了。')
    },
    RECEIVE_USER(state, userObj) {
      state.user = userObj
      console.log('RECEIVE_USER执行完了。')
    },
    RECEIVE_CANTEENLIST(state, canteens) {
      state.canteenList = canteens
      console.log('RECEIVE_CANTEENLIST执行完了。')
    },
    RECEIVE_CATEGORY(state, categories) {
      state.categoryList = categories
      // console.log(
      //   'mutations中的RECEIVE_CATEGORY被触发了，store中的categoryList已更新。',
      //   state.categoryList
      // )
    },
    RECEIVE_ADDRESS(state, addressObj) {
      state.order.address = addressObj
    },
    // 添加一个菜品进购物车
    ADD_DISH(state, dishObj) {
      state.cart[dishObj.dishId] = dishObj
      // 设置菜品数量 = 1
      state.cart[dishObj.dishId].amount = 1
      // 总计 + 菜品单价
      state.sum += dishObj.dishPrice
    },
    // 购物车里的菜品数量+1
    INCREASE_DISH(state, dishObj) {
      state.cart[dishObj.dishId].amount++
      // 总计 + 菜品单价
      state.sum += dishObj.dishPrice
    },
    // 删除一个菜品
    DELET_DISH(state, dishObj) {
      state.cart[dishObj.dishId].amount = 0
      delete state.cart[dishObj.dishId]
      state.sum -= dishObj.dishPrice
    },
    // 菜品数量-1
    MINUS_DISH(state, dishObj) {
      state.cart[dishObj.dishId].amount--
      state.sum -= dishObj.dishPrice
    },
    // 清空购物车对象并清零合计。
    CLEAR_CART(state) {
      // 购物车清空。
      state.cart = {}
      // 合计清零。
      state.sum = 0
      // categoryList中的菜品数量清零
      state.categoryList.forEach((category) => {
        category.dishes.forEach((dish) => {
          dish.amount = 0
        })
      })
    }
  },
  actions: {
    // 用户点击+1
    increment(context, dishObj) {
      console.log('actions中的increment被触发了。', context, dishObj.amount)
      // 如果 cart 已有当前菜品，就调整数量。不添加菜品对象。
      if (dishObj.amount > 0) {
        console.log('state.cart里已有当前菜品，数量+1')
        context.commit('INCREASE_DISH', dishObj)
      } else {
        console.log('state.cart里没有当前菜品，加入菜品，并且数量设置为1')
        context.commit('ADD_DISH', dishObj)
      }
      // 如果没有当前菜品，则添加菜品对象。
    },
    decrement(context, dishObj) {
      console.log('actions中的decrement被触发了。', context, dishObj.amount)
      // 如果菜品数量为1，那么移除菜品。
      if (dishObj.amount === 1) {
        // console.log('state.cart里当前菜品数量为1，从购物车中删除菜品。')
        context.commit('DELET_DISH', dishObj)
      } else {
        // console.log('state.cart里当前菜品数量-1')
        context.commit('MINUS_DISH', dishObj)
      }
    },
    async category(context, canteenId) {
      // console.log('actions中的category被触发了。接收到的食堂ID：', canteenId)
      let categories = []
      // 发送网络请求
      const res = await getCatList({ canteenId })
      if (res && res.status === 200 && res.data.code === 200) {
        categories = res.data.rows
        categories.forEach((category) => {
          category.text = category.catName
          getDishList({ catId: category.catId }).then((res) => {
            if (res.status === 200) {
              console.log('getDishList返回值：', res)
              category.dishes = res.data.rows
              category.dishes.forEach((dish) => {
                dish.amount = 0
              })
            }
          })
        })
        // console.log('actions中的category被触发了。categories', categories)
        context.commit('RECEIVE_CATEGORY', categories)
      }
    },
    async getCanteens(context) {
      const res = await getCanteenList()
      if (res && res.status === 200 && res.data.code === 200) {
        const tempCanteenList = res.data.rows
        // 查询每个食堂的orderCategoryList
        tempCanteenList.forEach((item) => {
          item.text = item.canteenName
          item.value = item.canteenId
          getOrderCategoryList(item.canteenId).then((res) => {
            if (res && res.status === 200 && res.data.code === 200) {
              item.children = res.data.data
              item.children.forEach((child) => {
                child.text = child.orderCatName
                child.value = child.orderCatId
              })
            } else {
              console.error('【Error】actions中的getCanteens方法：', res)
            }
          })
        })
        context.commit('RECEIVE_CANTEENLIST', tempCanteenList)
      } else {
        console.error('【Error】actions中的getCanteens方法：', res)
      }
    },
    async getUser(context) {
      const res = await getUser()
      console.log('---getUser---', res)
      if (res && res.status === 200 && res.data.code === 200) {
        context.commit('RECEIVE_USER', res.data.data)
      } else {
        console.error('【Error】---getUser---', res)
      }
    }
  }
})
