import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '@/views/Login'
import Layout from '@/views/Layout'
import Home from '@/views/Home'
import Dish from '@/views/Dish'
import Order from '@/views/Order'
import User from '@/views/User'
import CheckOut from '@/views/CheckOut'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/layout'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/layout',
    component: Layout,
    redirect: '/layout/home',
    children: [
      // 一级path需要'/'，二级path不需要
      {
        // 首页
        name: 'home',
        path: 'home',
        component: Home,
        meta: {
          // 元信息 -> 给当前路由对象绑定值
          title: '智慧食堂云订餐App'
        }
      },
      {
        // 点餐
        name: 'dish',
        path: 'dish',
        component: Dish,
        meta: {
          title: '点餐'
        }
      },
      {
        // 订单结算
        name: 'checkOut',
        path: 'checkOut',
        component: CheckOut,
        meta: {
          title: '订单结算'
        }
      },
      {
        // 订单
        name: 'order',
        path: 'order',
        component: Order,
        meta: {
          title: '我的订单'
        }
      },
      {
        // 我的
        name: 'user',
        path: 'user',
        component: User,
        meta: {
          title: '我的'
        }
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
