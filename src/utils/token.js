// 此文件 -> 封装3个方法 -> 专门用于操作token
// 封装东西，目的 -> 代码分层，更方便清晰，以后修改方便

const key = 'aero-fun-token'
// 设置
export const setToken = (token) => {
  localStorage.setItem(key, token)
}
// 获取
export const getToken = () => localStorage.getItem(key)
// 删除
export const delToken = () => {
  localStorage.removeItem(key)
}
