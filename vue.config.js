const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: true,
  lintOnSave: false
})

const path = require('path')
module.exports = {
  // 配置html文件中的htmlWebpackPlugin.options.title
  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      args[0].title = '智慧食堂云订餐App'
      return args
    })
  },

  css: {
    loaderOptions: {
      less: {
        lessOptions: {
          modifyVars: {
            // 直接覆盖变量
            // 'nav-bar-background-color': '#0ecccc',
            // 'nav-bar-title-text-color': '#ffffff'
            // 或者可以通过 less 文件覆盖（文件路径为绝对路径）
            // __dirname（两个下划线），它是node环境下全局内置变量
            // 当前文件所在文件夹的绝对路径
            hack: `true; @import "${path.join(
              __dirname,
              '/src/styles/vant_theme_modify.less'
            )}";`
          }
        }
      }
    }
  }
}
