import Vue from 'vue'

// 全局注册vant组件（按需引入）
import {
  NavBar, // 顶部导航
  Form,
  Field,
  Button,
  Tabbar, // 底部导航栏
  TabbarItem, // 底部导航栏
  Icon,
  Swipe, // 图片轮播
  SwipeItem, // 图片轮播
  Lazyload, // 图片懒加载
  Grid,
  GridItem,
  Image,
  Divider,
  Calendar,
  Picker,
  Popup,
  Cell,
  CellGroup,
  Cascader,
  Card,
  Tab,
  Tabs,
  Tag,
  DatetimePicker,
  Col,
  Row,
  List,
  NoticeBar,
  Empty,
  SwipeCell,
  PullRefresh,
  Uploader,
  Loading,
  Collapse,
  CollapseItem,
  Pagination,
  TreeSelect, // 分类选择
  Stepper,
  SubmitBar,
  ActionSheet,
  Dialog
} from 'vant'
Vue.use(NavBar)
  .use(TreeSelect)
  .use(Form)
  .use(Field)
  .use(Button)
  .use(Tabbar)
  .use(TabbarItem)
  .use(Icon)
  .use(Swipe)
  .use(SwipeItem)
  .use(Lazyload)
  .use(Grid)
  .use(GridItem)
  .use(Image)
  .use(Divider)
  .use(Calendar)
  .use(Picker)
  .use(Popup)
  .use(Cell)
  .use(CellGroup)
  .use(Cascader)
  .use(Card)
  .use(Tab)
  .use(Tabs)
  .use(Tag)
  .use(DatetimePicker)
  .use(Col)
  .use(Row)
  .use(List)
  .use(NoticeBar)
  .use(Empty)
  .use(SwipeCell)
  .use(PullRefresh)
  .use(Uploader)
  .use(Loading)
  .use(Collapse)
  .use(CollapseItem)
  .use(Pagination)
  .use(Stepper)
  .use(SubmitBar)
  .use(ActionSheet)
  .use(Dialog)
