### vue-cli脚手架初始化项目

#### 创建项目

```bash
vue create aero-fun-app
```

<img src="C:\仙女\工作\项目\云订餐项目\截图\Snipaste_2023-06-14_09-11-30.png" style="zoom: 50%;" />

<img src="C:\仙女\工作\项目\云订餐项目\截图\Snipaste_2023-06-14_09-12-15.png" style="zoom: 50%;" />

<img src="C:\仙女\工作\项目\云订餐项目\截图\Snipaste_2023-06-14_09-12-30.png" style="zoom:50%;" />

<img src="C:\仙女\工作\项目\云订餐项目\截图\Snipaste_2023-06-14_09-13-45.png" style="zoom:50%;" />



#### 项目目录结构

- node_modules文件夹：项目依赖文件夹
- public文件夹：一般放置静态资源（如图片），注意：放在public文件夹中的静态资源，webpack进行打包的时候，会原封不动打包到dist文件夹。
- src文件夹（源代码文件夹）：
  - assets文件夹：一般也是放置静态资源（一般放置多个组件公用的静态资源），注意，放置在assets文件夹下的静态资源，在webpack打包的时候，webpack会把静态资源当作一个模块，打包在JS文件里面。
  - components文件夹：一般放置的是非路由组件（全局组件）。
  - App.vue：唯一的根组件，Vue中的组件（.vue）
    - template：Html结构层
    - script：js行为层
    - style：css样式层
  - main.js：程序入口文件，整个程序中最先执行的文件。
- babel.config.js：配置文件（babel相关）
- package.json文件：认为是项目的身份证，记录项目名称、项目中有哪些依赖。
- package-lock.json：缓存性文件。

##### 新增文件夹

在src目录中补充创建以下目录：

  - /api	# 网络请求接口封装模块
    - index.js	# 封装请求方法
  - /styles	# 样式文件目录
  - /utils	# 工具模块目录

### 项目其他配置

- Prettier 插件设置（代码格式化）

  --- 在根目录下，创建 `.prettierrc` 文件。
  
  ```json
  {
    "semi": false,
    "singleQuote": true,
    "trailingComma": "none"
  }
  ```
  
- 项目运行起来的时候，让浏览器自动打开

  --- pakage.json
  
  ```json
  {
      "scripts": {
          "serve": "vue-cli-service serve --open",
          "build": "vue-cli-service build",
          "lint": "vue-cli-service lint"
      }
  }
  ```
  


- eslint校验功能关闭
  --- 在根目录下，创建 ` vue.config.js ` 文件。
  比如：声明变量但是没有使用，eslint校验工具报错。

  ```js
  module.exports = defineConfig({
      lintOnSave: false
  })
  ```

- eslint关闭名称校验
  
  > 在根目录下，创建 ` .eslintrc.js ` 文件。
  
  ```js
  module.exports = {
    extends: ["plugin:vue/essential", "@vue/standard"],
    rules: {
      "space-before-function-paren": 0, // 方法前空格
      "vue/multi-word-component-names": "off", // 关闭名称校验
    },
  }
  ```
  
- src文件夹简写方法，配置别名@（@等同于src文件夹的绝对路径）
  --- 在根目录下，创建 `jsconfig.json` 文件。

  ```json
  {
  	"compilerOptions": {
          "baseUrl": "./",
          "paths": {
            "@/*": ["src/*"]     
          }		
  	}
  }
  ```

### 项目Git存储

#### 新项目 - 本地仓库

- 创建本地仓库

  脚手架项目，默认有个git本地仓库，可以覆盖/删除.git文件夹自己创建。

  ```bash
  git init
  ```

- 暂存和提交

  注意：会在本地.git文件夹里，有一次本地的提交记录。

  ```bash
  git add.
  git commit -m '提交的说明'
  ```

#### 新项目 - 远程仓库

- 在gitee新建项目仓库，名为aero-fun-app。

- 代码保存到远程。

  ```bash
  git remote add origin https://gitee.com/yvonne_sweetie/aero-fun-app.git
  git push -u origin "master"
  ```

#### 以后提交并推送

- 先在本地提交（暂不保存到远程仓库）

  ```bash
  git add .
  git commit -m '提交说明，一定要写清楚，方便以后阅读'
  ```

- 推送到远程

  ```bash
  git push
  ```

#### 克隆/拉取

> 从远程git仓库，克隆代码到本地。
- 第一次克隆下来
  - http/https开头的地址，需要账号密码才能克隆。
  - .git@开头的地址，需要配置ssh密钥，才能免账号密码克隆。
```bash
git clone 远程git仓库地址
```
- 多人协同开发一个项目，对于他人推送的代码，直接拉取更新。
> 如果提示有冲突，打开代码，找到对方商量合并冲突。
```bash
git pull
```


### 项目_Axios

- 下载axios

  ```bash
  yarn add axios
  ```

### 项目_Vant组件库

- 下载vant组件库（vant2.x最新版本）

  ```bash
  yarn add vant@latest-v2
  ```

- 下载插件，自动按需引入

  babel-plugin-import 是一款 babel 插件，它会在编译过程中将 import 的写法自动转换为按需引入的方式。
  
  ```bash
  yarn add babel-plugin-import -D
  ```


- 在babel.config.js添加如下配置

  ```js
  module.exports = {
    plugins: [
      [
        'import',
        {
          libraryName: 'vant',
          libraryDirectory: 'es',
          style: true
        },
        'vant'
      ]
    ]
  }
  ```


### 项目_移动端适配

1. 下载 amfe-flexible

   > 根据网页宽度，设置html的font-size
   ```bash
   yarn add amfe-flexible
   ```
   
2. 在 main.js 文件中引入
   ```js
   import 'amfe-flexible'
   ```
   
3. 下载 postcss 和 postcss-pxtorem@5.1.1
   > postcss：后处理css，编译翻译css代码
   > postcss-pxtorem：把css代码里所有的px计算转换成rem
   ```bash
   yarn add postcss postcss-pxtorem@5.1.1
   ```

4. 根目录下创建 postcss.config.js 文件
   > 对 postcss 进行设置

   ```js
   module.exports = {
     plugins: {
       'postcss-pxtorem': {
         // 能够把所有元素的px单位转成Rem
         // rootValue: 转换px的基准值。
         // 编码时, 一个元素宽是75px，则换成rem之后就是2rem
         rootValue: 37.5,
         propList: ['*']
       }
     }
   }
   ```

### 项目路由分析

- Home：首页
- Dish：点餐
- Order：订单
- User：我的

### 开发

- 页面跳转
  - 不带参数,直接跳转
    ```js
    this.$router.push('/testDemo')
    ```
  - 带参数跳转
    ```js
    this.$router.push({path:'/testDemo',query:{setid:123456}})
    this.$router.push({name:'testDemo',params:{setid:111222}})
    ```
  - 接收参数
    ```js
    this.$route.params.setid
    ```
- 类型转换
  - 转换成浮点型
    ```js
    parseFloat() 将把前面分析合法的数字字符全部转换为数值并返回。
    ```

